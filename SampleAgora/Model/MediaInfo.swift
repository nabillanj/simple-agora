//
//  MediaInfo.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 04/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit

struct MediaInfo {
    var dimension = CGSize.zero
    var fps = 0
    
    init() {}
    
    init(dimension: CGSize, fps: Int) {
        self.dimension = dimension
        self.fps = fps
    }
    
    func description() -> String {
        return "\(Int(dimension.width))×\(Int(dimension.height)), \(fps)fps"
    }
}

extension CGSize {
    static func defaultDimension() -> CGSize {
        return AgoraVideoDimension640x360
    }
    
    static func validDimensionList() -> [CGSize] {
        return [AgoraVideoDimension160x120,
                AgoraVideoDimension240x180,
                AgoraVideoDimension320x240,
                AgoraVideoDimension640x360,
                AgoraVideoDimension640x480,
                AgoraVideoDimension960x720]
    }
}

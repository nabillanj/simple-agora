//
//  ChatMessage.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 05/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

struct ChatMessage {
    var account: String!
    var message: String!
}

struct ChatMessageList {
    var identifier: String!
    var list = [ChatMessage]()
}

var chatMessages = Dictionary<String, ChatMessageList>()
var userColor = Dictionary<String, UIColor>()

//
//  AgoraSignal.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 05/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import Foundation
import AgoraSigKit

struct AgoraSignal{
    static let Kit: AgoraAPI = AgoraAPI.getInstanceWithoutMedia(AgoraAppId)
}

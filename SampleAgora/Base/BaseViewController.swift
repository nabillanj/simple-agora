//
//  BaseViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 03/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    //MARK: LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Navigations
    
    func instantiateViewController(storyboardName: String, controllerName: String) -> UIViewController{
        let storyboard = UIStoryboard.init(name: storyboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: controllerName) 

        return controller
    }
    
    func instantiateStoryboard(storyboardName: String) -> UIStoryboard{
        let storyboard = UIStoryboard.init(name: storyboardName, bundle: nil)

        return storyboard
    }
    
    func presentViewController(controller: UIViewController){
        self.navigationController?.present(controller, animated: true, completion: nil)
    }
    
    /// PopToViewController
    ///
    /// - Parameter controller: Destination Controller
    func popToViewController(controller: UIViewController){
        self.navigationController?.popToViewController(controller, animated: true)
    }
    
    ///  PopToRootController
    func popToRootViewController(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /// Push Navigation
    ///
    /// - Parameter controller: Destination Controller
    func pushToViewController(controller: UIViewController){
        let back = UIBarButtonItem()
        back.title = ""
        navigationItem.backBarButtonItem = back
        self.navigationController?.pushViewController(controller, animated: true)
    }

    //MARK: Alert
    func check(String: String) -> Bool {
        if String.isEmpty {
            alert(string: "The account is empty !")
            return false
        }
        if String.count > 128 {
            alert(string: "The accout is longer than 128 !")
            return false
        }
        if String.contains(" ") {
            alert(string: "The accout contains space !")
            return false
        }
        return true
    }
    
    func alert(string: String) {
        guard !string.isEmpty else {
            return
        }
        
        DispatchQueue.main.async(execute: {
            let alert = UIAlertController(title: nil, message: string, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        })
    }
}

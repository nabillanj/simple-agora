//
//  SettingVideoCallTableViewCell.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 04/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class SettingVideoCallTableViewCell: BaseTableViewCell {

    //MARK: Outlet
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: Variables
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: Custom Function
    func bind(with dimension: CGSize, isSelected: Bool) {
        titleLabel.text = "\(Int(dimension.width))x\(Int(dimension.height))"
        
        backgroundColor = isSelected ? UIColor(red: 153/255, green: 186/255, blue: 221/255, alpha: 1.0) : UIColor.white
    }

}

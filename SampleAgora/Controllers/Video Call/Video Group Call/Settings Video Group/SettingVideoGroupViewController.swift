//
//  SettingVideoGroupViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 04/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

protocol SettingVideoGroupDelegate: class{
    func settingsVC(_ settingsVC: SettingVideoGroupViewController, didSelectDimension dimension: CGSize)
}

class SettingVideoGroupViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: Variables
    var dimension: CGSize! {
        didSet {
            tableView?.reloadData()
        }
    }
    weak var delegate: SettingVideoGroupDelegate?
    fileprivate let dimensionList: [CGSize] = CGSize.validDimensionList()
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        let indexPath = NSIndexPath(row: 2, section: 0) as IndexPath
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions
    @IBAction func didClickOkButton(_ sender: Any) {
        if dimension == nil {
            delegate?.settingsVC(self, didSelectDimension: dimensionList[2])
        }else {
            delegate?.settingsVC(self, didSelectDimension: dimension)
        }
    }
    
}

//MARK: TableView
extension SettingVideoGroupViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingVideoCallTableViewCell", for: indexPath) as! SettingVideoCallTableViewCell
        
        let backgroundSelectionView = UIView()
        backgroundSelectionView.backgroundColor = UIColor(red: 153/255, green: 186/255, blue: 221/255, alpha: 1.0)
        cell.selectedBackgroundView = backgroundSelectionView
        
        let selectedDimension = dimensionList[indexPath.row]
        cell.bind(with: selectedDimension, isSelected: (selectedDimension == dimension))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return dimensionList.count
    }
}

extension SettingVideoGroupViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dimension = dimensionList[indexPath.row]
    }
}

//
//  EnterChannelVideoViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 04/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class EnterChannelVideoViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet weak var encryptionButton: UIButton!
    @IBOutlet weak var channelTextfield: UITextField!
    @IBOutlet weak var encryptionTextfield: UITextField!
    
    //MARK: Variables
    fileprivate var dimension = CGSize.defaultDimension()
    fileprivate var encryptionType = EncryptionType.xts128 {
        didSet {
            encryptionButton?.setTitle(encryptionType.description(), for: .normal)
        }
    }
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: Actions
    @IBAction func doSettingButtonClick(_ sender: Any) {
        let settingController = instantiateStoryboard(storyboardName: "VideoCallStoryboard").instantiateViewController(withIdentifier: "SettingVideoGroupViewController") as! SettingVideoGroupViewController
        settingController.delegate = self
        
        self.pushToViewController(controller: settingController)
    }
    
    @IBAction func doRoomNameTextFieldEditing(_ sender: UITextField) {
        if let text = sender.text , !text.isEmpty {
            let legalString = MediaCharacter.updateToLegalMediaString(from: text)
            sender.text = legalString
        }
    }
    
    @IBAction func doEncryptionTextFieldEditing(_ sender: UITextField) {
        if let text = sender.text , !text.isEmpty {
            let legalString = MediaCharacter.updateToLegalMediaString(from: text)
            sender.text = legalString
        }
    }
    
    @IBAction func doEncryptionTypePressed(_ sender: UIButton) {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        for encryptionType in EncryptionType.allValue {
            let action = UIAlertAction(title: encryptionType.description(), style: .default) { [weak self] _ in
                self?.encryptionType = encryptionType
            }
            sheet.addAction(action)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(cancel)
        sheet.popoverPresentationController?.sourceView = encryptionButton
        sheet.popoverPresentationController?.permittedArrowDirections = .up
        present(sheet, animated: true, completion: nil)
    }
    
    @IBAction func doJoinPressed(_ sender: UIButton) {
        enter(roomName: encryptionTextfield.text)
    }
}

private extension EnterChannelVideoViewController {
    func enter(roomName: String?) {
        guard let roomName = roomName , !roomName.isEmpty else {
            return
        }
        
        let controller = instantiateStoryboard(storyboardName: "VideoCallStoryboard").instantiateViewController(withIdentifier: "VideoCallGroupViewController") as! VideoCallGroupViewController
        controller.delegate = self
        controller.dimension = dimension
        controller.roomName = roomName
        controller.encryptionType = encryptionType
        controller.encryptionSecret = channelTextfield.text
        
        pushToViewController(controller: controller)
    }
}

extension EnterChannelVideoViewController: SettingVideoGroupDelegate {
    func settingsVC(_ settingsVC: SettingVideoGroupViewController, didSelectDimension dimension: CGSize) {
        self.dimension = dimension
        self.popToViewController(controller: self)
    }
}

extension EnterChannelVideoViewController: VideoCallGroupDelegate {
    func onCloseVideoCall(vcg: VideoCallGroupViewController) {
        self.popToViewController(controller: self)
    }
}

extension EnterChannelVideoViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case channelTextfield:     enter(roomName: textField.text)
        case encryptionTextfield:   textField.resignFirstResponder()
        default: break
        }
        
        return true
    }
}

//
//  ChatMessageTableViewCell.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 04/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class ChatMessageTableViewCell: BaseTableViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var contentViews: UIView!
    
    //MARK: Variables
    

    //MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Custom Functions
    func bind(message: Message){
        messageLabel.text = message.text
        contentViews.backgroundColor = message.type.color()
    }

}

//
//  LoginBroadcastPageViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 05/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit

class LoginBroadcastPageViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var roomNameTextField: UITextField!
    @IBOutlet weak var popoverSourceView: UIView!
    
    //MARK: Variables
    fileprivate var videoProfile = AgoraVideoDimension640x360

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Custom Button
    func showRoleSelection() {
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let broadcaster = UIAlertAction(title: "Broadcaster", style: .default) { [weak self] _ in
            self?.join(withRole: .broadcaster)
        }
        let audience = UIAlertAction(title: "Audience", style: .default) { [weak self] _ in
            self?.join(withRole: .audience)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(broadcaster)
        sheet.addAction(audience)
        sheet.addAction(cancel)
        sheet.popoverPresentationController?.sourceView = popoverSourceView
        sheet.popoverPresentationController?.permittedArrowDirections = .up
        present(sheet, animated: true, completion: nil)
    }
    
    func join(withRole role: AgoraClientRole) {
        let bcController = self.instantiateStoryboard(storyboardName: "BroadcastStoryboard").instantiateViewController(withIdentifier: "BroadcastViewController") as! BroadcastViewController
        bcController.clientRole = AgoraClientRole(rawValue: role.rawValue)!
        bcController.roomName = roomNameTextField.text
        bcController.videoProfile = AgoraVideoDimension320x240
        bcController.delegate = self
        self.pushToViewController(controller: bcController)
    }
    
    //MARK: Actions
    @IBAction func didClickLoginButton(_ sender: Any) {
        if let string = roomNameTextField.text , !string.isEmpty {
            showRoleSelection()
        }
    }
}

extension LoginBroadcastPageViewController: BroadcastDelegate {
    func closeBroadcast(_ bc: BroadcastViewController) {
        self.popToViewController(controller: self)
    }
}

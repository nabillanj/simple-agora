//
//  VoiceCallViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 03/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit

class VoiceCallViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var btnMute: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnVolume: UIButton!
    
    //MARK: Variables
    var agoraKit: AgoraRtcEngineKit!

    //MARK: View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeAgoraEngine()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        joinChannel()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        leaveChannel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions
    @IBAction func didClickMuteButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        agoraKit.muteLocalAudioStream(sender.isSelected)
    }
    
    @IBAction func didClickCloseButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        leaveChannel()
    }
    
    @IBAction func didClickVolumeButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        agoraKit.setEnableSpeakerphone(sender.isSelected)
    }
    
}

//MARK: Extension Agora Delegate
extension VoiceCallViewController: AgoraRtcEngineDelegate {
    
    /// To Initialize Agora/Set Delegate
    func initializeAgoraEngine(){
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId: AgoraAppId, delegate: self)
    }
    
    /// Join Channel to start Voice Call
    func joinChannel(){
        agoraKit.joinChannel(byToken: nil, channelId: "demoChannel", info: nil, uid: 0) {[unowned self] (sid, uid, elapsed) -> Void in
            self.agoraKit.setEnableSpeakerphone(true)
            UIApplication.shared.isIdleTimerDisabled = true
        }
    }
    
    /// This Function called when user tap close call
    func leaveChannel(){
        agoraKit.leaveChannel(nil)
        UIApplication.shared.isIdleTimerDisabled = false
        self.popToRootViewController()
    }
}


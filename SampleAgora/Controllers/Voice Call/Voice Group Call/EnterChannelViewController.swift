//
//  EnterChannelViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 04/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class EnterChannelViewController: BaseViewController {
    
    //MARK: Outlets
    @IBOutlet weak var enterChannelNameTf: UITextField!
    @IBOutlet weak var joinChannelBtn: UIButton!
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier , segueId == "joinChannel",
            let roomName = sender as? String else {
                return
        }
        
        let roomVC = segue.destination as! RoomViewController
        roomVC.roomName = roomName
        roomVC.delegate = self
    }
    
    //MARK: Actions
    @IBAction func didClickJoinChannelButton(_ sender: UIButton) {
        if enterChannelNameTf.text != "" {
            joinChannel(roomName: enterChannelNameTf.text)
        }
    }
    
    @IBAction func didChangeEnterChannelTextfield(_ sender: UITextField) {
        if let text = sender.text , !text.isEmpty {
            let legalString = MediaCharacter.updateToLegalMediaString(from: text)
            sender.text = legalString
        }
    }
}

//MARK: Custom Functions
private extension EnterChannelViewController {
    
    /// Custom Functions to get room name and push to other controller
    ///
    /// - Parameter roomName: value from textfield
    func joinChannel(roomName: String?){
        guard let name = roomName, !name.isEmpty else {
            return
        }
        
        performSegue(withIdentifier: "joinChannel", sender: name)
    }
}

//MARK: UITextfieldDelegate
extension EnterChannelViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            joinChannel(roomName: textField.text)
        }
        return true
    }
}

//MARK: RoomViewControllerDelegate
extension EnterChannelViewController: RoomViewDelegate {
    func roomNeedClose(roomVC: RoomViewController) {
        self.popToViewController(controller: self)
    }
}

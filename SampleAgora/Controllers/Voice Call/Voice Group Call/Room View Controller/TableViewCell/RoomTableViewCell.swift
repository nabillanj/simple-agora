//
//  RoomTableViewCell.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 04/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class RoomTableViewCell: BaseTableViewCell {

    //MARK: Variable
    @IBOutlet weak var roomNameLabel: UILabel!
    
    //MARK: Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK: Custom Functions
    func bind(log: String){
        roomNameLabel.text = log
    }

}

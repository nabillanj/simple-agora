//
//  ViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 03/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions
    @IBAction func didClickVoiceCallButton(_ sender: Any) {
        self.pushToViewController(controller: self.instantiateViewController(storyboardName: "VoiceCallStoryboard", controllerName: "VoiceCallViewController"))
    }
    
    @IBAction func didClickVoiceGroupCallButton(_ sender: Any) {
        self.pushToViewController(controller: self.instantiateViewController(storyboardName: "VoiceCallStoryboard", controllerName: "EnterChannelViewController"))
    }
    
    @IBAction func didClickVideoCallButton(_ sender: Any) {
        self.pushToViewController(controller: self.instantiateViewController(storyboardName: "VideoCallStoryboard", controllerName: "VideoCallViewController"))
    }
    
    @IBAction func didClickVideoGroupCallButton(_ sender: Any) {
        self.pushToViewController(controller: self.instantiateViewController(storyboardName: "VideoCallStoryboard", controllerName: "EnterChannelVideoViewController"))
    }
    
    @IBAction func didClickBroadcastButton(_ sender: Any) {
        self.pushToViewController(controller: self.instantiateViewController(storyboardName: "BroadcastStoryboard", controllerName: "LoginBroadcastPageViewController"))
    }
    
    @IBAction func didClickChatButton(_ sender: Any) {
        self.pushToViewController(controller: self.instantiateViewController(storyboardName: "ChatStoryboard", controllerName: "LoginChatPageViewController"))
    }
}


//
//  EnterGroupChatViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 05/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit

class EnterGroupChatViewController: BaseViewController {

    //MARK: Outlet
    @IBOutlet weak var channelTextfield: UITextField!
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "logout"), style: .plain, target: self, action: #selector(logout))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addAgoraSignalBlock()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let chatRoomVC = segue.destination as? GroupChatViewController, let account = sender as? String {
            chatRoomVC.channelName = account
        }
    }
    
    //MARK: Custom Function
    func addAgoraSignalBlock() {
        AgoraSignal.Kit.onChannelJoined = { (channelID) -> () in
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: "ShowChannelRoom", sender: self.channelTextfield.text)
            })
        }
        
        AgoraSignal.Kit.onChannelJoinFailed = { (channelID, ecode) -> () in
            self.alert(string: "Join channel failed with error: \(ecode.rawValue)")
        }
    }
    
    //MARK: Actions
    @objc func logout() {
        AgoraSignal.Kit.logout()
    }
    
    @IBAction func didClickChannelButton(_ sender: UIButton) {
        guard let channelName = channelTextfield.text else {
            return
        }
        if !check(String: channelName) {
            return
        }
        AgoraSignal.Kit.channelJoin(channelName)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

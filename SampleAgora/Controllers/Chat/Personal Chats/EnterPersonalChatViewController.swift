//
//  EnterPersonalChatViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 05/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import AgoraSigKit
import AgoraRtcEngineKit

class EnterPersonalChatViewController: BaseViewController {
    //MARK: Outlet
    @IBOutlet weak var chatAccountTextField: UITextField!

    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "logout"), style: .plain, target: self, action: #selector(logout))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addAgoraSignalBlock()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let chatRoomVC = segue.destination as? PersonalChatViewController, let account = sender as? String {
            chatRoomVC.account = account
            if chatMessages[chatRoomVC.account] != nil {
                chatRoomVC.messageList = chatMessages[chatRoomVC.account]!
            } else {
                chatRoomVC.messageList.identifier = chatRoomVC.account
            }
        }
    }
    
    //MARK: Custom Function
    func addAgoraSignalBlock() {
        AgoraSignal.Kit.onLogout = { [weak self] (ecode) -> () in
            DispatchQueue.main.async(execute: {
                self?.dismiss(animated: true, completion: nil)
            })
        }
        
        AgoraSignal.Kit.onMessageInstantReceive = { (account, uid, msg) -> () in
            if chatMessages[account!] == nil {
                let message = ChatMessage(account: account, message: msg)
                var messagelist = [ChatMessage]()
                messagelist.append(message)
                let chatMsg = ChatMessageList(identifier: account, list: messagelist)
                chatMessages[account!] = chatMsg
                return
            }
            let message = ChatMessage(account: account, message: msg)
            chatMessages[account!]?.list.append(message)
        }
    }
    
    //MARK: Actions
    @objc func logout() {
        AgoraSignal.Kit.logout()
    }
    
    @IBAction func chatButtonClicked(_ sender: UIButton) {
        guard let chatAccount = chatAccountTextField.text else {
            return
        }
        if !check(String: chatAccount) {
            return
        }
        self.performSegue(withIdentifier: "ShowChatRoom", sender: chatAccount)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}



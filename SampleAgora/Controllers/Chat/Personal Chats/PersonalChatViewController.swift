//
//  PersonalChatViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 05/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import AgoraSigKit
import AgoraRtcEngineKit

class PersonalChatViewController: BaseViewController {
    //MARK: Outlet
    @IBOutlet weak var chatRoomContainView: UIView!
    @IBOutlet weak var chatRoomContainBottom: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var inputTextField: UITextField!
    
    //MARK: Variables
    var account: String!
    var isFirstLoad = true
    var timer: Timer!
    
    var isOnline = false {
        didSet {
            DispatchQueue.main.async(execute: {
                self.title = self.account + " (" + (self.isOnline ? "online" : "offline") + ")"
            })
        }
    }
    
    var messageList = ChatMessageList()
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        checkStatus()
        timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(checkStatus), userInfo: nil, repeats: true)
        
        addTouchEventToTableView(self.tableView)
        addKeyboardObserver()
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addAgoraSignalBlock()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if messageList.list.count == 0 {
            return
        }
        let indexPath = IndexPath(row: self.messageList.list.count - 1, section: 0)
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        chatMessages[account] = messageList
        timer?.invalidate()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Custom Function
    func addAgoraSignalBlock() {
        AgoraSignal.Kit.onQueryUserStatusResult = { [weak self] (name, status) -> () in
            if (self?.isFirstLoad)! {
                self?.isOnline = status == "1" ? true : false
                self?.isFirstLoad = false
                return
            }
            if status == "1" {
                if !(self?.isOnline)! {
                    self?.isOnline = true
                    return
                }
            } else {
                if (self?.isOnline)! {
                    self?.isOnline = false
                    return
                }
            }
        }
        
        AgoraSignal.Kit.onMessageSendSuccess = { [weak self] (messageID) -> () in
            let myAccount = UserDefaults.standard.string(forKey: "myAccount")
            DispatchQueue.main.async(execute: {
                let message = ChatMessage(account: myAccount, message: self?.inputTextField.text)
                self?.messageList.list.append(message)
                self?.updateTableView((self?.tableView)!, with: message)
                self?.inputTextField.text = ""
            })
        }
        
        AgoraSignal.Kit.onMessageSendError = { [weak self] (messageID, ecode) -> () in
            self?.alert(string: "Message send failed with error: \(ecode.rawValue)")
        }
        
        AgoraSignal.Kit.onMessageInstantReceive = { [weak self] (account, uid, msg) -> () in
            if account == self?.account {
                DispatchQueue.main.async(execute: {
                    let message = ChatMessage(account: account, message: msg)
                    self?.messageList.list.append(message)
                    self?.updateTableView((self?.tableView)!, with: message)
                    self?.inputTextField.text = ""
                })
            } else {
                if chatMessages[account!] == nil {
                    let message = ChatMessage(account: account, message: msg)
                    var messagelist = [ChatMessage]()
                    messagelist.append(message)
                    let chatMsg = ChatMessageList(identifier: account, list: messagelist)
                    chatMessages[account!] = chatMsg
                    return
                }
                let message = ChatMessage(account: account, message: msg)
                chatMessages[account!]?.list.append(message)
            }
        }
    }
    
    func addKeyboardObserver() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillShow, object: nil, queue: nil) { [weak self] notify in
            guard let strongSelf = self, let userInfo = (notify as NSNotification).userInfo,
                let keyBoardBoundsValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue,
                let durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber else {
                    return
            }
            
            let keyBoardBounds = keyBoardBoundsValue.cgRectValue
            let duration = durationValue.doubleValue
            let deltaY = keyBoardBounds.size.height
            
            if duration > 0 {
                var optionsInt: UInt = 0
                if let optionsValue = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    optionsInt = optionsValue.uintValue
                }
                let options = UIViewAnimationOptions(rawValue: optionsInt)
                
                UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
                    strongSelf.chatRoomContainBottom.constant = deltaY
                    strongSelf.view?.layoutIfNeeded()
                }, completion: nil)
                
            } else {
                strongSelf.chatRoomContainBottom.constant = deltaY
            }
            if strongSelf.messageList.list.count > 0 {
                let insterIndexPath = IndexPath(row: strongSelf.messageList.list.count - 1, section: 0)
                strongSelf.tableView.scrollToRow(at: insterIndexPath, at: .bottom, animated: false)
            }
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIKeyboardWillHide, object: nil, queue: nil) { [weak self] notify in
            guard let strongSelf = self else {
                return
            }
            
            let duration: Double
            if let userInfo = (notify as NSNotification).userInfo, let durationValue = userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber {
                duration = durationValue.doubleValue
            } else {
                duration = 0
            }
            
            if duration > 0 {
                var optionsInt: UInt = 0
                if let userInfo = (notify as NSNotification).userInfo, let optionsValue = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber {
                    optionsInt = optionsValue.uintValue
                }
                let options = UIViewAnimationOptions(rawValue: optionsInt)
                
                UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
                    strongSelf.chatRoomContainBottom.constant = 0
                    strongSelf.view?.layoutIfNeeded()
                }, completion: nil)
                
            } else {
                strongSelf.chatRoomContainBottom.constant = 0
            }
        }
    }
    
    @objc func checkStatus() {
        AgoraSignal.Kit.queryUserStatus(account)
    }
    
    func updateTableView(_ tableView: UITableView, with message: ChatMessage) {
        let indexPath = IndexPath(row: tableView.numberOfRows(inSection: 0), section: 0)
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .none)
        tableView.endUpdates()
        tableView.scrollToRow(at: indexPath, at: .bottom, animated: false)
    }
    
    func addTouchEventToTableView(_ tableView: UITableView) {
        let tableViewGesture = UITapGestureRecognizer(target: self, action: #selector(tableViewTouchInSide))
        tableViewGesture.numberOfTapsRequired = 1
        tableViewGesture.cancelsTouchesInView = false
        tableView.addGestureRecognizer(tableViewGesture)
        tableView.keyboardDismissMode = .onDrag
    }
    
    @objc func tableViewTouchInSide() {
        inputTextField.resignFirstResponder()
    }

}

//MARK: TableView
extension PersonalChatViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageList.list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if messageList.list[indexPath.row].account == account && account != UserDefaults.standard.string(forKey: "myAccount") {
            if userColor[account] == nil {
                userColor[account] = UIColor.randomColor()
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMessageLeftTableViewCell", for: indexPath) as! ChatMessageLeftTableViewCell
            cell.bind(message: messageList.list[indexPath.row])
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatMessageRightTableViewCell", for: indexPath) as! ChatMessageRightTableViewCell
        cell.bind(message: messageList.list[indexPath.row])
        return cell
    }
}

extension PersonalChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

//MARK: UITextField Delegate
extension PersonalChatViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let name = textField.text, !name.isEmpty, !name.trimmingCharacters(in: .whitespaces).isEmpty {
            guard let message = inputTextField.text else { return false }
            AgoraSignal.Kit.messageInstantSend(account, uid: 0, msg: message, msgID: String(messageList.list.count))
            
            return true
        }
        
        return true
    }
}

//
//  LoginChatPageViewController.swift
//  SampleAgora
//
//  Created by nabilla nurjannah on 05/12/18.
//  Copyright © 2018 Nabilla Nurjannah. All rights reserved.
//

import UIKit
import AgoraRtcEngineKit
import AgoraSigKit

class LoginChatPageViewController: BaseViewController {
    
    //MARK: Outlet
    @IBOutlet weak var accoutTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addAgoraSignalBlock()
        AgoraSignal.Kit.logout()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Actions
    @IBAction func didClickLoginButton(_ sender: Any) {
        guard let account = accoutTextField.text else {
            return
        }
        if !check(String: account) {
            return
        }
        
        AgoraSignal.Kit.login2(AgoraAppId, account: account, token: "_no_need_token", uid: 0, deviceID: nil, retry_time_in_s: 60, retry_count: 5)
        
        let lastAccount = UserDefaults.standard.string(forKey: "myAccount")
        if lastAccount != account {
            let userDefault = UserDefaults.standard
            userDefault.set(account, forKey: "myAccount")
            chatMessages.removeAll()
        }
        
        if userColor[account] == nil {
            userColor[account] = UIColor.randomColor()
        }
    }
    
    //MARK: Custom Function
    func addAgoraSignalBlock() {
        AgoraSignal.Kit.onLoginSuccess = { [weak self] (uid,fd) -> () in
            DispatchQueue.main.async(execute: {
                self?.performSegue(withIdentifier: "loginSegue", sender: self)
            })
        }
        
        AgoraSignal.Kit.onLoginFailed = { [weak self] (ecode) -> () in
            self?.alert(string: "Login failed with error: \(ecode.rawValue)")
        }
        
        AgoraSignal.Kit.onLog = { (txt) -> () in
            guard var log = txt else {
                return
            }
            let time = log[..<log.index(log.startIndex, offsetBy: 10)]
            let dformatter = DateFormatter()
            let timeInterval = TimeInterval(Int(time)!)
            let date = Date(timeIntervalSince1970: timeInterval)
            dformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            log.replaceSubrange(log.startIndex..<log.index(log.startIndex, offsetBy: 10), with: dformatter.string(from: date) + ".")
            
            LogWriter.write(log: log)
        }
        
        AgoraSignal.Kit.onMessageInstantReceive = { (account, uid, msg) -> () in
            if chatMessages[account!] == nil {
                let message = ChatMessage(account: account, message: msg)
                var messagelist = [ChatMessage]()
                messagelist.append(message)
                let chatMsg = ChatMessageList(identifier: account, list: messagelist)
                chatMessages[account!] = chatMsg
                return
            }
            let message = ChatMessage(account: account, message: msg)
            chatMessages[account!]?.list.append(message)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}
